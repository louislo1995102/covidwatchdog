import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("users").del();
    await knex("compulsorytesting599j").del();
    await knex("covidplace").del();
    await knex("news").del();
    await knex("covidhistory").del();
    await knex("過去14天").del();
    await knex("fourteendays").del();


    // Inserts seed entries
    await knex("users").insert([
        {
            name: "waylon",
            age: "26",
            phone: "+85298890071",
            address: "The Riverpark",
        },
    ]);

    await knex("covidhistory").insert([
        {
            EASTING: 823204,
            NORTHING: 819873,
            casenum: 12296,
            reportdate: "2021-10-16",
            DISTRICT_EN: "Islands",
            DISTRICT_TC: "離島",
            BLDGNAME_EN: "Designated Quarantine Facility (Penny's Bay Quarantine Centre)",
            BLDGNAME_TC: "指定檢疫設施（竹篙灣檢疫中心）",
            lat: 22.317806325,
            long: 114.050077099
        },
        {
            EASTING: 832345,
            NORTHING: 825551,
            casenum: 12297,
            reportdate: "2021-10-16",
            DISTRICT_EN: "Kwai Tsing",
            DISTRICT_TC: "葵青",
            BLDGNAME_EN: "Designated Quarantine Facility (Silka Tsuen Wan Hotel Hong Kong)",
            BLDGNAME_TC: "指定檢疫設施（香港荃灣絲麗酒店）",
            lat: 22.369126957,
            long: 114.138784353
        }
    ])
}