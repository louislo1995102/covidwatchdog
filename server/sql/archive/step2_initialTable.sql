BEGIN;


CREATE TABLE IF NOT EXISTS "user"
(
    address text,
    "addresslatlng" point,
    age smallint,
    id serial NOT NULL,
    img bytea,
    name text NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS "locationrecord"
(
    id bigserial NOT NULL,
    multilatlng point,
    "time" timestamp with time zone,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS covidplace
(
    id serial NOT NULL,
    createat timestamp with time zone,
    district text,
    buildingname text,
    casenum integer,
    visitdate timestamp with time zone,
    latitude double precision,
    longitude double precision,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS "casedetail"
(
    id serial NOT NULL,
    casenumber INTEGER NOT NULL,
    reportdate date,
    onsetdate date,
    gender text,
    age integer,
    "hospitalisedordischargedordeceased" text,
    situation text,
    resident text,
    "importorlocal" text,
    "probableorconfirmed" text,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS "covidplace_casedetail"
(
    covidplace_id serial NOT NULL,
    "casedetail_id" serial NOT NULL
);

CREATE TABLE IF NOT EXISTS "user_locationrecord"
(
    user_id serial NOT NULL,
    "locationrecord_id" bigserial NOT NULL
);

CREATE TABLE IF NOT EXISTS "locationrecord_covidplace"
(
    "locationrecord_id" bigserial NOT NULL,
    covidplace_id serial NOT NULL
);

CREATE TABLE IF NOT EXISTS "news"
(
    id bigserial,
    articles__source__name text, 
    articles__author text,
    articles__title text,
    articles__description text,
    articles__url text,
    articles__urlToImage text,
    articles__publishedAt text,
    articles__content text,
    PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS "covid19specimenscollection"
(
    id serial NOT NULL,
    "region" text,
    "clinic" text,
    "address" text,
    "tel" integer,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS "compulsorytesting599j"
(
    id serial NOT NULL,
    "hadbeenfrom" date,
    "hadbeento" date,
    deadline date,
    "passfrom" date,
    "passto" date,
    address text,
    latlng point,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS "compulsorytesting599j_locationrecord"
(
    "compulsorytesting599j_id" serial NOT NULL,
    "locationrecord_id" bigserial NOT NULL
);

ALTER TABLE "covidplace_casedetail"
    ADD FOREIGN KEY (covidplace_id)
    REFERENCES covidplace (id)
    NOT VALID;


ALTER TABLE "covidplace_casedetail"
    ADD FOREIGN KEY ("casedetail_id")
    REFERENCES "casedetail" (id)
    NOT VALID;


ALTER TABLE "user_locationrecord"
    ADD FOREIGN KEY (user_id)
    REFERENCES "user" (id)
    NOT VALID;


ALTER TABLE "user_locationrecord"
    ADD FOREIGN KEY ("locationrecord_id")
    REFERENCES "locationrecord" (id)
    NOT VALID;


ALTER TABLE "locationrecord_covidplace"
    ADD FOREIGN KEY ("locationrecord_id")
    REFERENCES "locationrecord" (id)
    NOT VALID;


ALTER TABLE "locationrecord_covidplace"
    ADD FOREIGN KEY (covidplace_id)
    REFERENCES covidplace (id)
    NOT VALID;


ALTER TABLE "compulsorytesting599j_locationrecord"
    ADD FOREIGN KEY ("compulsorytesting599j_id")
    REFERENCES "compulsorytesting599j" (id)
    NOT VALID;


ALTER TABLE "compulsorytesting599j_locationrecord"
    ADD FOREIGN KEY ("locationrecord_id")
    REFERENCES "locationrecord" (id)
    NOT VALID;

END;