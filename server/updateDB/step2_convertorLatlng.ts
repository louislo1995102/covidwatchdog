const axios = require("axios");
const fs = require("fs");

addTrueLatlng("599JDaily2021-10-15.geojson");

async function addTrueLatlng(fileNameAndExtension: string) {

    let rawdata = fs.readFileSync(`${__dirname}/output/${fileNameAndExtension}`);
    rawdata = JSON.parse(rawdata);

    const mutatedRawdata = rawdata.map((each: { properties: { EASTING: number; NORTHING: number; }; }) => {
        let ePair = { EASTING: each.properties.EASTING };
        let nPair = { NORTHING: each.properties.NORTHING };
        each = { ...each, ...ePair, ...nPair };
        return each;
    });
    const markerList = [];

    for (const marker of mutatedRawdata) {
        const latAndLong = await convertGPSLatlng(
            marker.properties.EASTING,
            marker.properties.NORTHING
        );
        markerList.push({
            ...marker,
            ...latAndLong,
        });
    }

    console.log(markerList);

    fs.writeFile(
        `${__dirname}/output/output_latlng${fileNameAndExtension}`,
        JSON.stringify(markerList),
        function (err: any) {
            if (err) {
                throw err;
            }
            console.log(`completed, added ${rawdata.length} Latlng(s) to ${fileNameAndExtension}. outputfile: output_latlng${fileNameAndExtension}. `);
        }
    );
}

async function convertGPSLatlng(e: number, n: number) {
    const res1 = await axios.get(
        `http://www.geodetic.gov.hk/transform/v2/?inSys=hkgrid&outSys=wgsgeog&e=${e}&n=${n}`
    );
    let lat = "";
    let long = "";
    console.log(res1);
    lat = res1.data.wgsLat;
    long = res1.data.wgsLong;
    console.log(lat, long);
    return {
        lat,
        long,
    };
}