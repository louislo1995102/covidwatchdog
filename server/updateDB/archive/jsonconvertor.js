const fs = require("fs");


//? generate dates Array satifying GEODATA GOV format.
function genDateArray(daysFromToday) {
    let newDate = new Date();
    let oldDate = new Date(newDate);
    let datesArray = [];
    for (let i = 0; i < daysFromToday; i++) {
        a = oldDate.setDate(newDate.getDate() - i);
        a = oldDate.toISOString().split("T")[0];
        datesArray.push(a);
    }
    newDate = newDate.toISOString().split("T")[0];
    oldDate = oldDate.toISOString().split("T")[0];
    console.log(datesArray);
    return datesArray;
}

//? parse JSON date by date
function gen599Jdaily(date,fileFullName) {
    
    let rawdata = fs.readFileSync(`${__dirname}/${fileFullName}`);
    rawdata = JSON.parse(rawdata);
    rawdata = rawdata.features;
    show = rawdata.filter((each) =>
    each.properties.GAZETTEDATE?.includes(date)
    );
    // console.log(show);
    // console.log(show.length);
    let data = JSON.stringify(show);
    fs.writeFileSync(`${__dirname}/599Jdaily${date}.geojson`, data);
    console.log(
        `completed. Regarding 599J, generated a file containing ${show.length} places on ${date} `
        );
}
    
genDateArray(19).map(eachDate=>gen599Jdaily(eachDate,"CHP_CAP599J_CTO_POINTS_V1_20211017.geojson"))