var axios = require("axios");
var Knex = require("knex");
var dotenv = require("dotenv");

// import axios from 'axios';
// import Knex from 'knex';
// import dotenv from 'dotenv';

dotenv.config();
const knexConfigs = require("../knexfile.ts");
const configMode = process.env.NODE_ENV || "development";
const knexConfig = knexConfigs[configMode];
const knex = Knex(knexConfig);
const API_KEY = "AIzaSyCo3vAHLG19_epHXtFaWWXOyqgbecz3rRg";



async function genTableAndLatLng() {
    //?get14days and insert
    let res;
    let 地區;
    let 大廈名單;
    let db大廈名單地區;
    let geo;
    let counter = 0;

    try {
        //? 過去14天內曾有疑似/確診個案居住過的大廈 https://api.data.gov.hk/v2/filter?q={"resource":"http://www.chp.gov.hk/files/misc/building_list_chi.csv","section":1,"format":"json","sorts":[[4,"desc"]]}
        res = await axios.get(
            "https://api.data.gov.hk/v2/filter?q=%7B%22resource%22%3A%22http%3A%2F%2Fwww.chp.gov.hk%2Ffiles%2Fmisc%2Fbuilding_list_chi.csv%22%2C%22section%22%3A1%2C%22format%22%3A%22json%22%2C%22sorts%22%3A%5B%5B4%2C%22desc%22%5D%5D%7D"
        );
    } catch (err) {
        console.error(err);
    }

    let inserting = res.data.map(
        async (each) =>
            await knex("過去14天")
                .insert([
                    {
                        地區: each.地區,
                        大廈名單: each.大廈名單,
                        個案最後到訪日期: each.個案最後到訪日期,
                        相關個案: each["相關疑似/確診個案"],
                    },
                ])
                .returning("id")
    );

    db大廈名單地區 = await knex("covidhistory").select(
        "DISTRICT_TC",
        "BLDGNAME_TC",
        "lat",
        "long"
    );

    console.log(res.data);
    console.log(`14日長度 i :${res.data.length}`);
    console.log(`db長度 j : ${db大廈名單地區.length}`);

    //14日要對db有無3個條件
    let needGoogle;
    const wtsupMan = res.data.map(async (each) => {
        return await knex("covidhistory")
            .where({ BLDGNAME_TC: each.大廈名單 })
            .where({ DISTRICT_TC: each.地區 })
            .whereNull("lat")
            .whereNull("long");
    });
    Promise.all(wtsupMan);

    console.log("睇下");
    console.log(wtsupMan[0]);
    console.log(wtsupMan);

    console.log(`要行google geocoder search ${wtsupMan[0]["地區"]}`);
    
    
    //! 呢舊野有d問題
    //! map唔可以async, 要promise all
    //全部放番入try catch.
    // const wtsupManLatlng = wtsupMan.map(async (each) => {
    //     try {
    //         let uri = encodeURI(each.大廈名單);
    //         geo = await axios.get(
    //             `https://maps.googleapis.com/maps/api/geocode/json?address=${uri}&key=${API_KEY}`
    //             // "https://maps.googleapis.com/maps/api/geocode/json?address=onemidtown&key=AIzaSyCo3vAHLG19_epHXtFaWWXOyqgbecz3rRg"
    //         );
    //         geo = geo.data;
    //         counter++;
    //     } catch (err) {
    //         console.error(err);
    //     }

    //     await knex("covidhistory").insert([
    //         {
    //             casenum: each.相關個案,
    //             BLDGNAME_TC: each.大廈名單,
    //             DISTRICT_TC: each.地區,
    //             lat: geo.results[0].geometry?.location.lat,
    //             long: geo.results[0].geometry?.location.lng,
    //         },
    //     ]);
    // });
    // Promise.all(wtsupManLatlng);
    
    // console.log("wtsupManLatlng" + wtsupManLatlng);
    


    // todo 拆左出黎 行then clause
    // for (let i = 0; i < res.data.length; i++) {
    //     for (let j = 0; j < db大廈名單地區.length; j++) {
    //         if (
    //             //對完data發覺 地區、大廈名單都岩, lat lng可以用, join table show
    //             //* 地區true + 大廈名true => 唔洗做野, 可skip 直接join table
    //             //* 如果中一半 false=> 要行latlng.
    //             //* 如果false & false => 要行latlng
    //             !(
    //                 res.data[i].地區 === db大廈名單地區[j].DISTRICT_TC &&
    //                 res.data[i].大廈名單 === db大廈名單地區[j].BLDGNAME_TC
    //             )
    //         ) {
    //             console.log(`要行google geocoder search ${res.data[i].大廈名單} ${res.data[i].地區}vs DB:${db大廈名單地區[j].DISTRICT_TC}, ${res.data[i].大廈名單}vs DB:${db大廈名單地區[j].BLDGNAME_TC}`);
    //             try {
    //                 let uri = encodeURI(res.data[i].大廈名單)
    //                 geo = await axios.get(
    //                     `https://maps.googleapis.com/maps/api/geocode/json?address=${uri}&key=${API_KEY}`
    //                     // "https://maps.googleapis.com/maps/api/geocode/json?address=onemidtown&key=AIzaSyCo3vAHLG19_epHXtFaWWXOyqgbecz3rRg"
    //                 );
    //                 geo = geo.data
    //                 counter++;
    //             } catch (err) {
    //                 console.error(err);
    //             }
    //             await knex("covidhistory").insert([
    // 				{
    // 					casenum: res.data[i].相關個案,
    //                     BLDGNAME_TC: res.data[i].大廈名單,
    //                     DISTRICT_TC: res.data[i].地區,
    //                     lat: geo.results[0].geometry?.location.lat,
    //                     long: geo.results[0].geometry?.location.lng,
    //                 },
    //             ]);
    // 			console.log(`${res.data[i].大廈名單}:${geo.results[0].geometry.location.lat},${geo.results[0].geometry.location.lng}`);
    //         }
    //     }
    // }
    console.log(`run api ${counter} times.`);
    //? 最後行呢段 genjoin Table
    gen過去14日latlng();
}

async function gen過去14日latlng() {
    let 過去14日latlng = await knex("過去14天")
        .join(
            "covidhistory",
            "過去14天.大廈名單",
            "=",
            "covidhistory.BLDGNAME_TC"
        )
        .select(
            "過去14天.地區",
            "過去14天.大廈名單",
            "過去14天.個案最後到訪日期",
            "過去14天.相關個案",
            "covidhistory.lat",
            "covidhistory.long"
        );
    console.log("過去14日latlng generated!");
    return 過去14日latlng;
}

genTableAndLatLng();