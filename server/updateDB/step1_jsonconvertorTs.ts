import fs from 'fs';

genDateArray(15).map(eachDate=>gen599Jdaily(eachDate,"CHP_CAP599J_CTO_POINTS_V1_20211017.geojson"))

//? generate dates Array satifying GEODATA GOV format.
function genDateArray(daysFromToday: number) {
    let newDate:any = new Date();
    let oldDate:any = new Date(newDate);
    let datesArray = [];
    for (let i = 0; i < daysFromToday; i++) {
        let a = oldDate.setDate(newDate.getDate() - i);
        a = oldDate.toISOString().split("T")[0];
        datesArray.push(a);
    }
    newDate = newDate.toISOString().split("T")[0];
    oldDate = oldDate.toISOString().split("T")[0];
    console.log(datesArray);
    return datesArray;
}

//? parse JSON date by date
function gen599Jdaily(date: any, fileNameAndFileExtension: fs.PathOrFileDescriptor) {
    let rawdata:any = fs.readFileSync(`${__dirname}/download/${fileNameAndFileExtension}`);
    rawdata = JSON.parse(rawdata);
    rawdata = rawdata.features;
    const show = rawdata.filter((each: { properties: { GAZETTEDATE: string | any[]; }; }) =>
        each.properties.GAZETTEDATE?.includes(date)
    );

    let data = JSON.stringify(show);
    fs.writeFileSync(`${__dirname}/output/599JDaily${date}.geojson`, data);
    console.log(
        `completed. Regarding 599J, generated a file containing ${show.length} places on ${date} `
    );
}
