const axios = require( "axios");
const fs = require( "fs");
const { getFourteendays } = require("../fetchToDB");

addTrueLatlng("locaDB.json");

async function addTrueLatlng(fileNameAndExtension) {

    let rawdata = fs.readFileSync(`${__dirname}/download/${fileNameAndExtension}`);
    rawdata = JSON.parse(rawdata);

    const mutatedRawdata = rawdata.map((each) => {
        let ePair = { EASTING: each.EASTING };
        let nPair = { NORTHING: each.NORTHING };
        each = { ...each, ...ePair, ...nPair };
        return each;
    });
    const markerList = [];

    for (const marker of mutatedRawdata) {
        const latAndLong = await convertGPSLatlng(
            marker.EASTING,
            marker.NORTHING
        );
        markerList.push({
            ...marker,
            ...latAndLong,
        });
    }

    console.log(markerList);

    fs.writeFile(
        `${__dirname}/output/output_latlng${fileNameAndExtension}`,
        JSON.stringify(markerList),
        function (err) {
            if (err) {
                throw err;
            }
            console.log(`completed, added ${rawdata.length} Latlng(s) to ${fileNameAndExtension}. outputfile: output_latlng${fileNameAndExtension}. `);
        }
    );
}

let i = 0

async function convertGPSLatlng(e, n) {
    const res1 = await axios.get(
        `http://www.geodetic.gov.hk/transform/v2/?inSys=hkgrid&outSys=wgsgeog&e=${e}&n=${n}`
    );
    let lat = "";
    let long = "";
    // console.log(res1);
    lat = res1.data.wgsLat;
    long = res1.data.wgsLong;
    // console.log(lat, long);
    console.log("processing: " , i++)
    return {
        lat,
        long,
    };
}
