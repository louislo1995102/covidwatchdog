import { ReportService } from "../services/ReportServices";
import { Request, Response } from 'express'


export class ReportController {
    public constructor(private reportService: ReportService) {}

    report = async (req: Request, res: Response) => {
        try {
            const { data, title } = req.body;
            let name = data.name
            let content = data.content
            await this.reportService.addReportedProblems(name, content, title)
            return res.json({isSuccess: true})
        } catch (e) {
            console.log(e)
            return res.status(500).json({error: 'Unknown error'})
        }
    }
}