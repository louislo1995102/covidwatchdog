import {Request, Response} from 'express';
import {LocationService} from '../services/LocationService';
//todo import {Board} from '../services/models';

export class LocationController {
  constructor(private locationService: LocationService) {}

  getAll = async (req: Request, res: Response) => {
    try {
      //todo 改type
      const results: any = await this.locationService.allLocation();
      console.log(`results in location controller: ${results}`);
      //todo 係呢度砌json
      res.json({isSuccess: true, data: results.map((row: any) => row)});
    } catch (e) {
      res.json({isSuccess: false, msg: {e}.toString()});
    }
  };

  // get = async (req: Request, res: Response) => {
  //   try {
  //     const id = parseInt(req.params.id);
  //     if (isNaN(id)) {
  //       res.status(400).json({msg: 'Id is not a number'});
  //       return;
  //     }
  //     const result: any = await this.boardService.getBoard(id);
  //     res.json({isSuccess: true, data: result});
  //   } catch (e) {
  //     res.json({isSuccess: false, msg: e.toString()});
  //   }
  // };
}
