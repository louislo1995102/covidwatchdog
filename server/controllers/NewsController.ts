import {Request, Response} from 'express';
import {NewsService} from '../services/NewsService';
//todo import {Board} from '../services/models';

export class NewsController {
  constructor(private newsService: NewsService) {}

  getAll = async (req: Request, res: Response) => {
    try {
      //todo 改type
      const results: any = await this.newsService.allNews();
      console.log(`results in news controller: ${results[1].articles__description}`);
      console.log(`results in news controller: ${results[1].articles__urltoimage}`);
      //todo 係呢度砌json
      res.json({isSuccess: true, data: results.map((row: any) => row)});
    } catch (e) {
      res.json({isSuccess: false, msg: {e}.toString()});
    }
  };
}
