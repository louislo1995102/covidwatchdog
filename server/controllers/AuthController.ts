import { UserService } from "../services/UserService";
import { Request, Response } from 'express';


import { initializeApp } from 'firebase-admin/app';
const admin = require('firebase-admin');
let serviceAccount = require("../firebase/covidappotp-d2775-firebase-adminsdk-9d50d-511b8f77b1.json")

initializeApp({
    credential: admin.credential.cert(serviceAccount),
});  

export class AuthController {
    public constructor(private userService: UserService) {

    }
    
    login = async (req: Request, res: Response ) => {
        try {
            const token = req.body.token
            let decodedToken = await admin.auth().verifyIdToken(token)
            const user = await this.userService.getUserByPhone(decodedToken.phone_number)
            console.log(user)
            // return res.json({ user: user})
            if (!user) {
                console.log('please register')
                return res.status(201).json({msg: 'Please go register'})
            } else {
                return res.status(200).json({user: user})
            }
        } catch (e) {
            console.log(e);
            return res.status(500).json({error: 'Unknown error'})
        }
    }
}