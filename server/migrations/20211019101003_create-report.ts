import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('problem-reports');
    if (!hasTable) {
        return await knex.schema.createTable('problem-reports',table=> {
            table.increments();
            table.string('name').notNullable();
            table.string('content').notNullable()
            table.string('categories').notNullable()
            table.timestamps(false, true);
        })
    } else {
        return Promise.resolve()
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('problem-reports')
}

