import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('fourteendays');
    if (!hasTable) {
        return await knex.schema.createTable('fourteendays',table=> {
            table.increments();
            table.string("place");
            table.float("lat");
            table.float("long");
            table.string("confirmID");
            table.timestamps(false, true)
        })
    } else {
        return Promise.resolve()
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable("fourteendays");
}