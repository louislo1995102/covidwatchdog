import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('過去14天');
    if(!hasTable){
        await knex.schema.createTable('過去14天',(table)=>{
            table.increments();
            table.text("地區");
            table.text("大廈名單");
            table.text("個案最後到訪日期");
            table.integer("相關個案");
        });  
    }
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('過去14天');
}

