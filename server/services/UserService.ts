import { Knex } from 'knex';

export class UserService {
    public constructor(private knex: Knex){

    }
    public async getUserByPhone(phone: string) {
        return (await this.knex.raw(`SELECT * FROM users WHERE phone = ?`, [phone])).rows[0]
    }

    public async getUserById(id: number) {
        return (await this.knex.raw(`SELECT * FROM users WHERE id = ?`, [id])).rows[0]
    }
}