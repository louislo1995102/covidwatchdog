import {Knex} from 'knex';
// import {強檢公告} from './models';

export class NewsService {
  constructor(private knex: Knex) {}
  allNews() {
    return this.knex('news');
  }
}
