import { Knex } from 'knex';

export class ReportService {
    public constructor (private knex: Knex){}
    
    public async addReportedProblems(name:string, content: string, categories: string) {
        return (await this.knex.insert({name, content, categories}).into('problem-reports'))
    }
}