// import {Point} from 'react-native-maps';

//todo work左再執model
// export interface 強檢公告 {
//   id?: number;
//   deadline?: Date;
//   address: string;
//   Latlng: Point;
// }

// export interface Board {
//   id?: number;
//   squares: Array<string | null>;
// }

// export interface Score {
//   winner: string;
//   name: string;
//   squares: Array<string | null>;
// }

export interface User{
    id:number
    name: string
    age: number
    phone: string
    address: string
}


declare global{
    namespace Express{
        interface Request{
            user?: User
        }
    }
}