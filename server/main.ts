import express from 'express';
import cors from 'cors';
import Knex from 'knex';
import dotenv from 'dotenv'
import {LocationService} from './services/LocationService';
import {LocationController} from './controllers/LocationController';

import {NewsService} from './services/NewsService';
import {NewsController} from './controllers/NewsController';

import { UserService } from './services/UserService';
import { AuthController } from './controllers/AuthController';

import { ReportService } from './services/ReportServices';
import { ReportController } from './controllers/ReportController';

dotenv.config()

const knexConfig = require('./knexfile')
const knex = Knex(knexConfig[process.env.NODE_ENV || 'development']);

const app = express();

app.use(cors());
app.use(express.urlencoded({extended: true}));
app.use(express.json());

const locationService = new LocationService(knex);
export const locationController = new LocationController(locationService);

const newsService = new NewsService(knex);
export const newsController = new NewsController(newsService);

const userService = new UserService(knex)
export const authController = new AuthController(userService)

const reportService = new ReportService(knex)
export const reportController = new ReportController(reportService)

import {routes} from './routes';

app.use('/', routes);

const PORT = 8080;
app.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}/`);
  if (process.env.CI) {
    process.exit(0);
  }
});
