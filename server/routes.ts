import express from 'express';
import {authController, locationController, newsController, reportController} from './main';


export const routes = express.Router();

routes.get('/past14days', locationController.getAll);
routes.get('/news', newsController.getAll);

routes.post('/login', authController.login)

routes.post('/report', reportController.report)