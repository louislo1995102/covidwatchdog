import express from 'express'
import { Bearer } from 'permit'
import { UserService } from './services/UserService'
import jwtSimple from 'jwt-simple'
import jwt from './jwt'

export const createIsLoggedIn = (permit: Bearer, userService: UserService) => {
    return async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            const token = permit.check(req);

            if (!token) {
                return res.status(401).json({error: 'Permission Denied'})
            }

            const payload = jwtSimple.decode(token, jwt.jwtSecret)
            const user = await userService.getUserById(payload.id)

            if (!user) {
                return res.status(401).json({error: 'Permission Denied'})
            }

            req.user = user;

            return next();
        } catch (e) {
            console.log(e)
            return res.status(401).json({error: 'Permission Denied'})
        }
    }
} 