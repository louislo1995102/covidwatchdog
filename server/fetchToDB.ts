import Knex from 'knex';
import dotenv from 'dotenv';
dotenv.config()
const knexConfigs = require('./knexfile');
const configMode = process.env.NODE_ENV || "development";
const knexConfig = knexConfigs[configMode]
const knex = Knex(knexConfig)

import fetch from "node-fetch"

const API_KEY = "AIzaSyCo3vAHLG19_epHXtFaWWXOyqgbecz3rRg"
export async function getLocation(address:any,confirmID:any){
    let res=await fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${API_KEY}`)
    let json:any=await res.json();
    // console.log(json)
    // console.log(json.results[0].address_compnents)
    console.log(address+":"+json.results[0].geometry.location.lat+","+json.results[0].geometry.location.lng+";"+"confirmation ID =" + confirmID)
    await knex
      .insert({
        place: address,
        lat: json.results[0].geometry.location.lat,
        long: json.results[0].geometry.location.lng,
        confirmID: confirmID
        
      })
      .into("fourteendays");
    
}



export async function getFourteendays(){
    let MyDate = new Date();
    let MyDateString;
    MyDate.setDate(MyDate.getDate());
    MyDateString = MyDate.getFullYear() + ''
        + ('0' + (MyDate.getMonth() + 1)).slice(-2) + ''
        //  + ('0' + MyDate.getDate()).slice(-2);
        + ('20').slice(-2);
    console.log(MyDateString)

    let FourteenDate = new Date();
    let FourteenDateString;
    FourteenDate.setDate(FourteenDate.getDate() - 9);

    FourteenDateString = ('0' + (FourteenDate.getMonth())).slice(-2) + ''
        + ('21').slice(-2);
    console.log(FourteenDateString);

    fetch(
        `https://api.data.gov.hk/v1/historical-archive/get-file?url=http%3A%2F%2Fwww.chp.gov.hk%2Ffiles%2Fmisc%2Fbuilding_list_chi.csv&time=${MyDateString}-${FourteenDateString}`
    ).then((r) => {
        r.text().then((d) => {
            let fourteen = d.replace('地區,大廈名單,個案最後到訪日期,相關疑似/確診個案', '').replace('\r', '')
            let data = fourteen.split('\n');
            data.shift()
            data.pop()
            console.log(data)
            for (let x of data) {
                let confirmID = x.substring(x.length-6).slice(0,-1)
                let newX = x.slice(0, -8)
                console.log(newX)
                console.log(getLocation(newX, confirmID))
            }

        });
    });
}

getFourteendays()