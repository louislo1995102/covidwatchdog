import React from 'react';
import { Linking, ScrollView, StyleSheet, Text, View } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { Dimensions, Platform, PixelRatio } from 'react-native';

const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');


// @ts-ignore
// import Modal from 'react-native-modal';
// import ModalBaseScene from './assets/utils/ModalBaseScene';

export default function Diagnosis() {
  return (
    <ScrollView>

    <View style={styles.container}>
      <View style={styles.banner}>
      <Text style={styles.title}><FontAwesome5 name="lungs-virus" size={30} color="red" /> 症狀</Text>
      </View>
      <View style={styles.item_sub_bg}>
        <Text style={{fontSize: 20}}><FontAwesome5 name="lungs-virus" size={25} color="red" /> 患者通常會有以下症狀</Text>
        <Text style={{fontSize: 17, marginBottom: 25}}>（以百分比呈現）</Text>
        <View style={styles.item_title_bg}>
          <Text style={styles.item_left}>發燒</Text>
          <Text style={[styles.item_right, {paddingLeft: 130} ]}>98.6%</Text>
        </View>
        <View style={styles.item_title_bg}>
          <Text style={styles.item_left}>乏力</Text>
          <Text style={[styles.item_right, {paddingLeft: 70} ]}>69.6%</Text>
        </View>
        <View style={styles.item_title_bg}>
          <Text style={styles.item_left}>乾咳</Text>
          <Text style={[styles.item_right, {paddingLeft: 60} ]}>59.4%</Text>
        </View>
        <View style={styles.item_title_bg}>
          <Text style={styles.item_left}>肌痛</Text>
          <Text style={[styles.item_right, {paddingLeft: 35} ]}>34.8%</Text>
        </View>
        <View style={styles.item_title_bg}>
          <Text style={styles.item_left}>呼吸短促</Text>
          <Text style={[styles.item_right, {paddingLeft: 30} ]}>31.2%</Text>
        </View>
        <View style={styles.item_title_bg}>
          <Text style={styles.item_left}>咳痰</Text>
          <Text style={[styles.item_right, {paddingLeft: 25} ]}>26.8%</Text>
        </View>
        <View style={styles.item_title_bg}>
          <Text style={styles.item_left}>頭痛</Text>
          <Text style={[styles.item_right, {paddingLeft: 7} ]}>6.5%</Text>
        </View>
        <Text style={{fontSize: 17, marginTop: 20}} onPress={()=>Linking.openURL('https://jamanetwork.com/journals/jama/fullarticle/2761044?guestAccessKey=f61bd430-07d8-4b86-a749-bec05bfffb65')}>資料來源: 美國醫學會雜誌</Text>
      </View>
    </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  banner: {
    height: 70,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  title: {
    fontSize: 30,
    color: 'black'
  },
  item_title_bg: {
    alignItems: 'center',
    flexDirection: 'row',
    margin: 5,
    marginBottom: 15,
  },
  item_sub_bg: {
    paddingHorizontal: 15,
    paddingVertical: 15,
    width: '100%',
    alignItems: 'center',
    height: '90%'
  },
  item_left: {
    fontSize: 25,
    color: 'black',
    flex: 1,
  },
  item_right: {
    backgroundColor: 'red',
    fontSize: 25,
    color: 'white',
    borderRadius: 5,
    overflow: 'hidden',
    paddingRight: 5
  },
})

