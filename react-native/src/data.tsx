import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet ,ScrollView} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import axios from 'axios';
////////////////////////////////////////////////////////
import { Dimensions, Platform, PixelRatio } from 'react-native';

const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');
////////////////////////////////////////////////////////
interface Case {
  'As of date': string;
  'As of time': string;
  'Number of confirmed cases': number;
  'Number of ruled out cases': string;
  'Number of cases still hospitalised for investigation': string;
  'Number of cases fulfilling the reporting criteria': string;
  'Number of death cases': number;
  'Number of discharge cases': number;
  'Number of probable cases': number;
  'Number of hospitalised cases in critical condition': number;
}



export default function Data() {
  const [data, getdata] = useState<Case | null>(null);
 
  useEffect(() => {
    fetchdata();
  }, []);

  async function fetchdata() {
    const result = await axios(
      'https://api.data.gov.hk/v2/filter?q=%7B%22resource%22%3A%22http%3A%2F%2Fwww.chp.gov.hk%2Ffiles%2Fmisc%2Flatest_situation_of_reported_cases_covid_19_eng.csv%22%2C%22section%22%3A1%2C%22format%22%3A%22json%22%2C%22sorts%22%3A%5B%5B5%2C%22asc%22%5D%2C%5B8%2C%22asc%22%5D%2C%5B10%2C%22asc%22%5D%5D%2C%22filters%22%3A%5B%5B3%2C%22ge%22%2C%5B%2212250%22%5D%5D%2C%5B7%2C%22ge%22%2C%5B%22200%22%5D%5D%5D%7D',
    );
    const data: Case[] = result.data;
    const dataSortedByDate = data.sort((a, b) => {
      if (
        new Date(a['As of date']).getTime() >
        new Date(b['As of date']).getTime()
      ) {
        return -1;
      }
      if (
        new Date(a['As of date']).getTime() >
        new Date(b['As of date']).getTime()
      ) {
        return 1;
      }
      return 0;
    });
    const latestData = dataSortedByDate[data.length - 1];
    getdata(latestData);
  }
  
  return (
    <ScrollView>

    <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text style={styles.date}>地區：香港{"\n"}更新日期：{data?.["As of date"]}</Text>
      </View>
      <View style={styles.largeContainer}>
        <Text style={styles.datestyle}>累積確診個案: {data?.["Number of confirmed cases"]}</Text>
      </View>
      <View style={[styles.container2, {
        flexDirection: "row"
      }]}>
        <Text style={styles.square}>
          死亡個案:{"\n"} <MaterialCommunityIcons
            name="skull"
            size={SCREEN_WIDTH/22}
          /> {data?.["Number of death cases"]}
        </Text>
        <Text style={styles.square2}>
          出院：{"\n"}
          <MaterialCommunityIcons
            name="hospital"
            size={SCREEN_WIDTH/22}
          /> {data?.["Number of discharge cases"]}
        </Text>
      </View>
      <View style={[styles.container3, {
        flexDirection: "row"
      }]}>
        <Text style={styles.square3}>
          疑似個案：{"\n"}
          <MaterialCommunityIcons
            name="percent"
            size={SCREEN_WIDTH/22}
          /> {data?.["Number of probable cases"]}
        </Text>
        <Text style={styles.square4}>
          危殆個案:{"\n"}
          <MaterialCommunityIcons
            name="emoticon-dead-outline"
            size={SCREEN_WIDTH/22}
          /> {data?.["Number of hospitalised cases in critical condition"]}
        </Text>
      </View>
    </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom:30,
    alignItems:'center'
  },
  titleContainer: {
    padding: 20,
    height: SCREEN_HEIGHT/5,
  },
  date: {
    fontSize: SCREEN_WIDTH/20,
    textAlign: "center",
    color: 'black',
    textShadowColor: 'black',
    textShadowRadius: 9,
    textShadowOffset: {width: 2, height: 2}
  },
  largeContainer: {
    alignItems: 'center',
    width:SCREEN_WIDTH/1.1,
  },
  datestyle: {
    fontSize: 38,
    textAlign: "center",
    backgroundColor: 'black',
    color: 'lightgreen',
    padding: 50,
    borderRadius: 10,
    overflow: 'hidden',
    width: '90%',
    justifyContent: 'center'
  },
  container2: {
    padding: 20,
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  container3: {
    padding: 20,
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  square: {
    backgroundColor: "black",
    textAlign: "center",
    fontSize: SCREEN_WIDTH/22,
    padding: 30,
    width: SCREEN_WIDTH/2.5,
    height: SCREEN_HEIGHT/5,
    margin: 4,
    borderRadius: 10,
    overflow: 'hidden',
    color: 'red'
  },
  square2: {
    backgroundColor: "black",
    textAlign: "center",
    fontSize: SCREEN_WIDTH/22,
    color: 'white',
    padding: 30,
    width: SCREEN_WIDTH/2.5,
    height: SCREEN_HEIGHT/5,
    margin: 4,
    borderRadius: 10,
    overflow: 'hidden',
  },
  square3: {
    backgroundColor: "black",
    textAlign: "center",
    fontSize: SCREEN_WIDTH/22,
    color: 'yellow',
    padding: 30,
    width: SCREEN_WIDTH/2.5,
    height: SCREEN_HEIGHT/5,
    margin: 4,
    borderRadius: 10,
    overflow: 'hidden',
  },
  square4: {
    backgroundColor: "black",
    textAlign: "center",
    fontSize: SCREEN_WIDTH/22,
    color: 'green',
    padding: 30,
    width: SCREEN_WIDTH/2.5,
    height: SCREEN_HEIGHT/5,
    margin: 4,
    borderRadius: 10,
    overflow: 'hidden',
  },
});