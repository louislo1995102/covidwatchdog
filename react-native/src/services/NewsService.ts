import {Article} from '../model/Article';

export class NewsService {
  private readonly apiKey = '41927d303e214cd0ae7f617acef69d24';

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  getTopHeadlinesByCountry(countryCode: string): Promise<Article[]> {
    let newday: String | Date = new Date();
    let oldday: String | Date = new Date(newday);
    oldday.setDate(newday.getDate() - 5);
    newday = newday.toISOString().split('T')[0];
    oldday = oldday.toISOString().split('T')[0];
    console.log(`news component: fetching ${oldday} to ${newday}`);

    return fetch(
      `https://newsapi.org/v2/everything?excludeDomains=dapenti.com,zhihu.com,shinobi.jp,inmediahk.net&q=香港肺炎&from=${oldday}&to=${newday}&sortBy=publishedAt&apiKey=41927d303e214cd0ae7f617acef69d24`,
    )
      .then(response => response.json())
      .then(function (results) {
        return results.articles;
      })
      .then(function (articles) {
        //   let newArticles=[];
        //  for(let article of articles){
        //    if(article.description.includes('肺炎')||article.title.includes('肺炎')||article.description.includes('疫情')||article.title.includes('疫情')){
        //     newArticles.push(article)
        //    }

        //  }
        return articles as Article[];
      });
  }
}
// const puppeteerExport = require('./lib/cjs/puppeteer/node');
//vs//shake//debug

//todo 9e37024414e34cb0a4125e334daf3253  //randy's key
//todo 41927d303e214cd0ae7f617acef69d24  //louis' key
//randy testing
// https://newsapi.org/v2/everything?q=香港肺炎&apiKey=41927d303e214cd0ae7f617acef69d24

// let newday: String | Date = new Date();
// let oldday: String | Date = new Date(newday);
// oldday.setDate(newday.getDate() - 5);
// newday = newday.toISOString().split('T')[0];
// oldday = oldday.toISOString().split('T')[0];
// console.log(`news component: fetching ${oldday} to ${newday}`);
