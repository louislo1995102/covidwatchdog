import { REACT_APP_BACKEND_URL } from '@env';
import { Dispatch } from 'redux';
import { AuthActions } from './actions';

export function login(token: string) {
  return async (dispatch: Dispatch<AuthActions>) => {
    const res = await fetch(`${REACT_APP_BACKEND_URL}/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ token }),
    });

    const result = await res.json();

    if (res.status !== 200) {
        console.log('go register')
    } else {
        console.log(result.user)
    }
  };
}

export function report(data: any, title: string) {
  return async () => {
    const res = await fetch(`${REACT_APP_BACKEND_URL}/report`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ data, title }),
    });
  }
}