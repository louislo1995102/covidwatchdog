import { CallHistoryMethodAction } from "connected-react-router"

export interface User {
    id: number,
    phone: string
}

export function loginSuccess(token: string, user:User) {
    return {
        type: '@@auth/LOGIN_SUCCESS' as '@@auth/LOGIN_SUCCESS',
        token,
        user
    }
}

export function loginFailed() {
    return {
        type: '@@auth/LOGIN_FAILED' as '@@auth/LOGIN_FAILED'
    }
}

export type AuthActions = ReturnType<typeof loginSuccess | typeof loginFailed>