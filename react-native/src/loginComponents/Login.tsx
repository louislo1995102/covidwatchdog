import React from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet
} from 'react-native';
import PhoneInput from 'react-native-phone-number-input'

export default function Login(props: {
    phoneInput: any
    value: any
    setPhoneValue: any
    setFormattedValue: any
    validateNumber: any
}) {
    return (
        <View style={styles.modalView}>
            <Text style={styles.modalTitle}>登入</Text>
            <Text style={styles.modalTitle}>請輸入您的電話號碼</Text>
            <PhoneInput
                ref={props.phoneInput}
                defaultValue={props.value}
                defaultCode="HK"
                layout="first"
                onChangeText={(text) => {
                    props.setPhoneValue(text);
                }}
                onChangeFormattedText={(text) => {
                    props.setFormattedValue(text);
                }}
                autoFocus
                placeholder="請輸入電話號碼"
            />
            <TouchableOpacity
                style={styles.userButton}
                onPress={() => {
                    props.validateNumber()
                }}
            >
                <Text >確定</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    modalView: {
        width: '100%',
        height: '75%',
        backgroundColor: "grey",
        padding: 35,
        alignItems: "center",
        elevation: 5,
        alignContent: 'center'
    },
    modalTitle: {
        fontSize: 30,
        marginBottom: 10,
    },
    userButton: {
        height: 50,
        width: 200,
        borderWidth: 2,
        borderRadius: 15,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#5DB075',
        borderColor: '#5DB075',
        marginTop: 20,
    },
})
