import React from 'react';
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    StyleSheet
} from 'react-native';

export default function Otp(props: {
    code: any
    setCode: any
    confirmCode: any
    phoneNum: any
}) {
    return (
        <View style={styles.modalView}>
            <Text style={styles.modalTitle}>驗證</Text>
            <Text style={styles.modalTitle}>包含驗證碼的訊息已傳送至 {props.phoneNum}</Text>
            <TextInput value={props.code} onChangeText={text => props.setCode(text)} style={styles.input} keyboardType="numeric" autoFocus placeholder={'請輸入驗證碼'} />
            <TouchableOpacity onPress={() => props.confirmCode()} style={styles.userButton}><Text>確定</Text></TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    modalView: {
        width: '100%',
        height: '75%',
        backgroundColor: "grey",
        padding: 35,
        alignItems: "center",
        elevation: 5,
        alignContent: 'center'
    },
    modalTitle: {
        fontSize: 30,
        marginBottom: 10,
    },
    input: {
        backgroundColor: 'white',
        width: 300,
        height: 50,
        fontSize: 20,
        marginTop: 10,
        borderRadius: 10,
        paddingLeft: 10,
    },
    userButton: {
        height: 50,
        width: 200,
        borderWidth: 2,
        borderRadius: 15,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#5DB075',
        borderColor: '#5DB075',
        marginTop: 20,
    },
})