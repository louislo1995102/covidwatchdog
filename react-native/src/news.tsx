import {useEffect, useState} from 'react';
import React from 'react';
import ref from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TextInput,
  Button,
  FlatList,
  Image,
  Linking,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {Article} from './model/Article';
import {NewsService} from './services/NewsService';

export default function News() {
  ///////////////////speech recognition/////////////////////////////

  const [result, setResult] = useState('');
  const [isLoading, setLoading] = useState(false);

  //////////////////////////////////////////////////////////

  /////////////////fetch && search news function//////////////////////////////////////
  const [filteredData, setfilterData] = useState<Article[]>([]);
  const [masterData, setMasterData] = useState<Article[]>([]);

  useEffect(() => {
    fetchNews();

    return () => {};
  }, []);

  const newsService = new NewsService();
  const fetchNews = async () => {
    const articles = await newsService.getTopHeadlinesByCountry('hk');
    setfilterData(articles);
    setMasterData(articles);
  };

  const ItemView = ({item}: any) => {
    return (
      <View
        key={item.title}
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          borderWidth: 2,
          borderColor: 'gray',
          margin: 5,
          backgroundColor: 'white',
          borderRadius: 15,
        }}>
        <Text
          style={{
            fontSize: 15,
            margin: 5,
            fontStyle: 'normal',
            fontWeight: 'bold',
          }}>
          {item.title}
        </Text>
        <Image
          source={{uri: item.urlToImage}}
          style={{height: 200, width: '100%', flex: 1}}
          resizeMode={'contain'}
        />
        <Text>{item.description}</Text>
        <Text
          style={{margin: 5, color: 'blue', fontStyle: 'normal'}}
          onPress={() => Linking.openURL(item.url)}>
          點此進入新聞網頁
        </Text>
      </View>
    );
  };
  const ItemSeparatorView = () => {
    return (
      <View style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8'}} />
    );
  };


  return (
    <View>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <View
          style={{
            position: 'relative',
            margin: 5,
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
  
          <View>
            {isLoading ? (
              <ActivityIndicator
                size="large"
                color="red"
                style={{bottom: 35, left: 95}}
              />
            ) : (
              <TouchableOpacity
                style={{bottom: 32, left: 95}}>
                <Image
                  source={{
                    uri: 'https://raw.githubusercontent.com/AboutReact/sampleresource/master/microphone.png',
                  }}
                  style={{width: 25, height: 25}}
                />
              </TouchableOpacity>
            )}
          </View>

         
          <Text>共有{filteredData.length}個搜尋結果</Text>
        </View>
      </View>
      <FlatList
        data={filteredData}
        keyExtractor={(item, index) => index.toString()}
        ItemSeparatorComponent={ItemSeparatorView}
        renderItem={ItemView}
      />
    </View>
  );
}
