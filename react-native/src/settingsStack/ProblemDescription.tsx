import React from 'react';
import {
    View,
    Text,
    TextInput,
    Pressable,
    StyleSheet,
    TouchableOpacity,
    Alert,
    ScrollView
} from 'react-native';
import { useForm, Controller } from 'react-hook-form'
import { useDispatch } from 'react-redux'
import { report } from '../redux/auth/thunk';


export default function ProblemDescription({route}:any) {
    const { title } = route.params
    const dispatch = useDispatch()
    const { control, handleSubmit, reset, formState: { errors } } = useForm();
    const onSubmit = (data: any) => {dispatch(report(data, title)); Alert.alert('我們已收到你的回報。'); reset({name: '', content: ''})}

    return (
        <ScrollView>
        <View style={styles.container}>
            <View style={styles.banner}>
                <Text style={styles.title}>{title}</Text>
            </View>
            <Controller control = {control} rules={{required: true}} render ={({ field: { onChange, onBlur, value}}) => (
                <TextInput editable style={styles.textBox} onChangeText={onChange} value={value} onBlur={onBlur} placeholder="請輸入您的姓名。"/>
            )}
            name="name"
            defaultValue=""
            />
            <Controller control = {control} rules={{required: true}} render ={({ field: { onChange, onBlur, value}}) => (
                <TextInput editable style={styles.textBox} onChangeText={onChange} value={value} onBlur={onBlur} numberOfLines={10} multiline placeholder="簡短說明所發生的情況，以及我們應採取哪些步驟來重現問題。"/>
            )}
            name="content"
            defaultValue=""
            />
            <TouchableOpacity style={styles.userButton} onPress={handleSubmit(onSubmit)}><Text style={styles.submitText}>提交</Text></TouchableOpacity>
        </View>

        </ScrollView>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        background: 'white',
        alignItems: 'center',
        marginBottom: 10,
    },
    banner: {
        height: 70,
        justifyContent: 'center',
    },
    textBox: {
        borderColor: 'black',
        borderWidth: 2,
        borderRadius: 10,
        textAlignVertical: "top",
        fontSize: 20,
        marginTop: 20,
        marginBottom: 30,
        width: '85%',
        padding: 10,
    },
    title: {
        fontSize: 30,
        color: 'black'
    },
    userButton: {
        height: 50,
        width: 200,
        borderWidth: 2,
        borderRadius: 15,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#5DB075',
        borderColor: '#5DB075',
    },
    submitText: {
        fontSize: 25,
        color: 'white'
    }
})