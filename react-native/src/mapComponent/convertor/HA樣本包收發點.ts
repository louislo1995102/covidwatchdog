/* eslint-disable no-shadow */
/* eslint-disable @typescript-eslint/no-unused-vars */
import axios from 'axios';
const fs = require('fs');

async function extractActual() {
  let e = '';
  let n = '';
  //! 醫院管理局樣本收集包派發及收集點
  const res: any = await axios.get(
    'https://geodata.gov.hk/gs/api/v1.0.0/locationSearch?q=%E9%86%AB%E9%99%A2%E7%AE%A1%E7%90%86%E5%B1%80%E6%A8%A3%E6%9C%AC%E6%94%B6%E9%9B%86%E5%8C%85%E6%B4%BE%E7%99%BC%E5%8F%8A%E6%94%B6%E9%9B%86%E9%BB%9E',
  );
  console.log(res.data);

  const marker: any = res.data[0];

  for (let key in marker) {
    if (key === 'x') {
      e = marker[key];
    }
  }
  for (let key in marker) {
    if (key === 'y') {
      n = marker[key];
    }
  }

  const markerList = [];

  for (const marker of res.data) {
    const latAndLong = await getLatAndLong(marker.x, marker.y);
    markerList.push({
      ...marker,
      ...latAndLong,
    });
  }

  console.log(markerList);

  fs.writeFile(
    `${__dirname}/output/latlngHA樣本包收發點.ts`,
    JSON.stringify(markerList),
    function (err: Error) {
      if (err) {
        throw err;
      }
      console.log('complete');
    },
  );
}

async function getLatAndLong(e: string, n: string) {
  const res1: any = await axios.get(
    `http://www.geodetic.gov.hk/transform/v2/?inSys=hkgrid&e=${e}&n=${n}`,
  );
  let lat: any = '';
  let long: any = '';
  console.log(res1.data);
  lat = res1.data.hkLat;
  long = res1.data.hkLong;
  console.log(lat, long);
  return {
    lat,
    long,
  };
}

extractActual();
