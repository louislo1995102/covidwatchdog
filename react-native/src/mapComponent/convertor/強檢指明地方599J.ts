/* eslint-disable @typescript-eslint/no-unused-vars */
import axios from 'axios';
const fs = require('fs');

async function extractActual() {
  let e = '';
  let n = '';
  //! 強制檢測公告的指明地方
  const res: any = await axios.get(
    'https://geodata.gov.hk/gs/api/v1.0.0/locationSearch?q=%E5%BC%B7%E5%88%B6%E6%AA%A2%E6%B8%AC%E5%85%AC%E5%91%8A%E7%9A%84%E6%8C%87%E6%98%8E%E5%9C%B0%E6%96%B9',
  );
  console.log(res.data);

  const marker: any = res.data[0];

  for (let key in marker) {
    if (key === 'x') {
      e = marker[key];
    }
  }
  for (let key in marker) {
    if (key === 'y') {
      n = marker[key];
    }
  }

  const markerList = [];

  // eslint-disable-next-line no-shadow
  for (const marker of res.data) {
    const latAndLong = await getLatAndLong(marker.x, marker.y);
    markerList.push({
      ...marker,
      ...latAndLong,
    });
  }

  console.log(markerList);

  fs.writeFile(
    `${__dirname}/output/latlng強檢指明地方599J.ts`,
    JSON.stringify(markerList),
    function (err: Error) {
      if (err) {
        throw err;
      }
      console.log('complete');
    },
  );
}

async function getLatAndLong(e: string, n: string) {
  const res1: any = await axios.get(
    `http://www.geodetic.gov.hk/transform/v2/?inSys=hkgrid&e=${e}&n=${n}`,
  );
  let lat: any = '';
  let long: any = '';
  console.log(res1.data);
  lat = res1.data.hkLat;
  long = res1.data.hkLong;
  console.log(lat, long);
  return {
    lat,
    long,
  };
}

extractActual();
