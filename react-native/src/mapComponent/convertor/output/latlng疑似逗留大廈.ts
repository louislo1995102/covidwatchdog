export const 疑似逗留大廈 = [
  {
    addressZH: '灣仔告士打道7號入境事務大樓5樓',
    nameZH: '延期逗留組',
    x: 835862.43119288,
    y: 815656.91492851,
    nameEN: 'Extension Section',
    addressEN: '5/F, Immigration Tower, 7 Gloucester Road, Wan Chai',
    lat: 22.281313169,
    long: 114.170486406,
  },
  {
    addressZH: '香港灣仔告士打道7號入境事務大樓5樓',
    nameZH: 'Wi-Fi.HK 熱點位置: 入境事務處-延期逗留組',
    x: 835868.7,
    y: 815639,
    nameEN: 'Wi-Fi.HK Hotspot Locations: Immigration - Extension Section',
    addressEN: '5/F, Immigration Tower, 7 Gloucester Road, Wan Chai, Hong Kong',
    lat: 22.281151391,
    long: 114.170547241,
  },
];
