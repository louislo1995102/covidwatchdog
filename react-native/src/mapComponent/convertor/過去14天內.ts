/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-shadow */
import axios from 'axios';
const fs = require('fs');

async function extractActual() {
  let e = '';
  let n = '';
  //! 過去14天內曾有2019冠狀病毒疑似/確診個案居住過的大廈位置 (如有群組，則為28天)
  const res: any = await axios.get(
    'https://geodata.gov.hk/gs/api/v1.0.0/locationSearch?q=%E9%81%8E%E5%8E%BB14%E5%A4%A9%E5%85%A7%E6%9B%BE%E6%9C%892019%E5%86%A0%E7%8B%80%E7%97%85%E6%AF%92%E7%96%91%E4%BC%BC%2F%E7%A2%BA%E8%A8%BA%E5%80%8B%E6%A1%88%E5%B1%85%E4%BD%8F%E9%81%8E%E7%9A%84%E5%A4%A7%E5%BB%88%E4%BD%8D%E7%BD%AE',
  );
  console.log(res.data);

  const marker: any = res.data[0];

  for (let key in marker) {
    if (key === 'x') {
      e = marker[key];
    }
  }
  for (let key in marker) {
    if (key === 'y') {
      n = marker[key];
    }
  }

  const markerList = [];

  for (const marker of res.data) {
    const latAndLong = await getLatAndLong(marker.x, marker.y);
    markerList.push({
      ...marker,
      ...latAndLong,
    });
  }

  console.log(markerList);

  fs.writeFile(
    `${__dirname}/output/latlng過去14天內.ts`,
    JSON.stringify(markerList),
    function (err: Error) {
      if (err) {
        throw err;
      }
      console.log('complete');
    },
  );
}

async function getLatAndLong(e: string, n: string) {
  const res1: any = await axios.get(
    `http://www.geodetic.gov.hk/transform/v2/?inSys=hkgrid&e=${e}&n=${n}`,
  );
  let lat: any = '';
  let long: any = '';
  console.log(res1.data);
  lat = res1.data.hkLat;
  long = res1.data.hkLong;
  console.log(lat, long);
  return {
    lat,
    long,
  };
}

extractActual();
