/* eslint-disable @typescript-eslint/no-unused-vars */
import axios from 'axios';
const fs = require('fs');

//! 2019冠狀病毒的社區檢測中心
async function extractActual() {
  let e = '';
  let n = '';
  const res: any = await axios.get(
    'https://geodata.gov.hk/gs/api/v1.0.0/locationSearch?q=2019%E5%86%A0%E7%8B%80%E7%97%85%E6%AF%92%E7%9A%84%E7%A4%BE%E5%8D%80%E6%AA%A2%E6%B8%AC%E4%B8%AD',
  );
  console.log(res.data);

  const marker: any = res.data[0];

  for (let key in marker) {
    if (key === 'x') {
      e = marker[key];
    }
  }
  for (let key in marker) {
    if (key === 'y') {
      n = marker[key];
    }
  }

  const markerList = [];

  // eslint-disable-next-line no-shadow
  for (const marker of res.data) {
    const latAndLong = await getLatAndLong(marker.x, marker.y);
    markerList.push({
      ...marker,
      ...latAndLong,
    });
  }

  console.log(markerList);

  fs.writeFile(
    `${__dirname}/output/latlng社區檢測中心.ts`,
    JSON.stringify(markerList),
    function (err: Error) {
      if (err) {
        throw err;
      }
      console.log('complete');
    },
  );
}

async function getLatAndLong(e: string, n: string) {
  const res1: any = await axios.get(
    `http://www.geodetic.gov.hk/transform/v2/?inSys=hkgrid&e=${e}&n=${n}`,
  );
  let lat: any = '';
  let long: any = '';
  console.log(res1.data);
  lat = res1.data.hkLat;
  long = res1.data.hkLong;
  console.log(lat, long);
  return {
    lat,
    long,
  };
}

extractActual();
