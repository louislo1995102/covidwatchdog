/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';

import Settings from './src/settings';
import Data from './src/data';
import News from './src/news';
import Map from './src/map';
import Diagnosis from './src/diagnosis';
import { store } from './store';
import { Provider } from 'react-redux';
import AccountSettings from './src/settingsStack/AccuntSettings';
import ReportProblem from './src/settingsStack/ReportProblem';
import ProblemDescription from './src/settingsStack/ProblemDescription';
import SplashScreen from 'react-native-splash-screen';
import { Dimensions } from 'react-native';

const {
  width: SCREEN_WIDTH
} = Dimensions.get('window');


const ReportStack = createNativeStackNavigator();

function ReportProblemStack() {
  return (
    <ReportStack.Navigator 
      initialRouteName='回報問題' 
      screenOptions={{ 
        headerTitleAlign: 'center', 
        headerTitleStyle: { fontSize: SCREEN_WIDTH / 15 } 
      }}>
      <ReportStack.Screen name='回報問題' component={ReportProblem} />
      <ReportStack.Screen name='傳送問題回報' component={ProblemDescription} />
    </ReportStack.Navigator>
  )
}

const InnerStack = createNativeStackNavigator()

function SettingsStack() {
  return (
    <InnerStack.Navigator 
      initialRouteName="設定"
      screenOptions={{
        headerTitleAlign: 'center',
        headerTitleStyle: { fontSize: SCREEN_WIDTH / 15 }
      }}>
      <InnerStack.Screen name="設定" component={Settings} />
      <InnerStack.Screen name="更改個人資料" component={AccountSettings} />
      <InnerStack.Screen name="ReportProblem" component={ReportProblemStack} options={{ headerShown: false }} />
    </InnerStack.Navigator>
  )
}
const Tab = createBottomTabNavigator();

const Mainpage = () => {
  return (
    <Tab.Navigator
      initialRouteName="地圖"
      backBehavior='initialRoute'
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color }) => {
          let iconName: string | undefined = 'black';

          if (route.name === '數據') {
            iconName = 'bar-chart-outline';
          } else if (route.name === '新聞') {
            iconName = 'newspaper-outline';
          } else if (route.name === '地圖') {
            iconName = 'location-outline';
          } else if (route.name === '診斷') {
            iconName = 'chatbox-ellipses-outline';
          } else if (route.name === 'Settings') {
            iconName = 'settings-outline';
          }
          return <Ionicons name={iconName} size={SCREEN_WIDTH / 15} color={color} />;
        },
        tabBarActiveTintColor: 'red',
        tabBarInactiveTintColor: 'gray',
        tabBarHideOnKeyboard: true,
        headerTitleAlign: 'center',
        headerTitleStyle: { fontSize: SCREEN_WIDTH / 15 }
      })}>
      <Tab.Screen name="地圖" component={Map} />
      <Tab.Screen name="診斷" component={Diagnosis} />
      <Tab.Screen name="數據" component={Data} />
      <Tab.Screen name="新聞" component={News} />
      <Tab.Screen name="Settings" component={SettingsStack} options={{ title: '設定', headerShown: false }} />
    </Tab.Navigator>
  );
};

const Stack = createNativeStackNavigator();

const App = () => {
  // const isAuthenicated = useSelector((state: RootState) => state.auth.isAuthenticated)
  useEffect(() => {
    return SplashScreen.hide();
  })
  return (

    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Main"
            component={Mainpage}
            options={{ headerShown: false }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider >
  );
};

export default App;
