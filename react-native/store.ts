import { combineReducers, createStore, compose, applyMiddleware } from 'redux';
import thunk, {ThunkDispatch as OldThunkDispatch} from 'redux-thunk'
import { authReducer, AuthState } from './src/redux/auth/reducer';
import { AuthActions } from './src/redux/auth/actions';


export interface RootState {
    auth: AuthState,
}

export type RootActions = AuthActions;

const reducers = combineReducers<RootState>({
    auth: authReducer,
})

declare global{
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
    }
}

export type ThunkDispatch = OldThunkDispatch<RootState, null, RootActions>

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore<RootState, RootActions, {}, {}>(reducers, composeEnhancers(
    applyMiddleware(thunk)
) )